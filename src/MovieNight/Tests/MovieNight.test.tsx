import React from 'react';
import { render } from '@testing-library/react';
import MovieNight from '../Components/MovieNight';

test('renders learn react link', () => {
  const { getByText } = render(<MovieNight />);
  const linkElement = getByText(/learn react/i);
  expect(linkElement).toBeInTheDocument();
});
