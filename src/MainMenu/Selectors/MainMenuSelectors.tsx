import ReduxState from '../../Redux/Types/ReduxState';

export const getMainMenuIsActiveFromState = (state: ReduxState) => state.mainMenu.isMainMenuActive;
export const getMovieNightRoomFromState = (state: ReduxState) => state.mainMenu.room;
export const getAlreadyAutoJoinedFromState = (state: ReduxState) =>
  state.mainMenu.alreadyAutoJoined;
export const getPublicRoomsFromState = (state: ReduxState) => state.mainMenu.publicRooms;
