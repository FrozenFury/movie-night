import { GoogleProfileState } from '../../GoogleProfile/Types/GoogleProfileState';
import { MainMenuState } from '../../MainMenu/Types/MainMenuState';
import { MovieNightState } from '../../MovieNight/Types/MovieNightState';
import { ServerState } from '../../Server/Types/ServerState';
import { MessageInputState } from '../../MessageInput/Types/MessageInputState';
import { SettingsPaneState } from '../../SettingsPane/Types/SettingsPaneTypes';

export default interface ReduxState {
  googleProfile: GoogleProfileState;
  mainMenu: MainMenuState;
  movieNight: MovieNightState;
  serverState: ServerState;
  messageInputState: MessageInputState;
  settingsPaneState: SettingsPaneState;
}
