import ReduxState from '../../Redux/Types/ReduxState';

export const getIsTypingFromState = (state: ReduxState): boolean => {
  return state.messageInputState.isTyping;
};
