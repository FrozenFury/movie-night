import { GoogleProfile } from '../../GoogleProfile/Types/GoogleProfile';

export default interface User {
  googleProfile: GoogleProfile;
  isTyping: boolean;
  isConnected: boolean;
}
