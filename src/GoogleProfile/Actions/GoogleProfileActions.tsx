import { GoogleLogOutActions } from '../Modules/LogOut/Actions/GoogleLogOutActions';
import { GoogleLogInActions } from '../Modules/LogIn/Actions/GoogleLogInActions';

export type GoogleProfileActions = GoogleLogInActions | GoogleLogOutActions;
