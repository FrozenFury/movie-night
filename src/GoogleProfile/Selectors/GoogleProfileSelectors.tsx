import ReduxState from '../../Redux/Types/ReduxState';

export const getGoogleProfileFromState = (state: ReduxState) => state.googleProfile.googleProfile;

export const getLoggedInStatusFromState = (state: ReduxState) => state.googleProfile.loggedIn;

export const getShouldInitiateAutoLoginFromState = (state: ReduxState) =>
  state.googleProfile.shouldInitiateAutoLogin;
