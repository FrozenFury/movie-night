import ReduxState from '../../Redux/Types/ReduxState';

export const getMessagesFromState = (state: ReduxState) => state.serverState.messages;
export const getUsersFromState = (state: ReduxState) => state.serverState.users;
export const getSocketFromState = (state: ReduxState) => state.serverState.socket;
export const getIsReadyFromState = (state: ReduxState) => state.serverState.isReady;
