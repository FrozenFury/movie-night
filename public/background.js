"use strict";

let globalToken = '';
const isValidURL = (url) => {
  return url.includes('youtube.com');
};

// Makes icon clickable only on YouTube
chrome.runtime.onInstalled.addListener(function(details) {
  chrome.declarativeContent.onPageChanged.removeRules(undefined, function() {
    chrome.declarativeContent.onPageChanged.addRules([{
      conditions: [
        new chrome.declarativeContent.PageStateMatcher({
          pageUrl: {
            hostEquals: 'www.youtube.com',
            pathPrefix: "/watch",
            schemes: ['http', 'https']
          }
        })
      ],
      actions: [new chrome.declarativeContent.ShowPageAction()]
    }]);
  });
});

// Send message to app when icon is clicked
chrome.pageAction.onClicked.addListener(function(tab){
  if (isValidURL(tab.url)) {
    chrome.tabs.sendMessage(tab.id, { type: 'Start', source: 'Clicked' });
  }
});

// Check if should automatically open
chrome.tabs.onUpdated.addListener(function(tabId, changeInfo, tab) {
  if (tab.url.includes('startMovieNightApp') && isValidURL(tab.url)) {
    chrome.tabs.sendMessage(tab.id, { type: 'Start', source: 'URL'});
  } else if (isValidURL(tab.url)) {
    chrome.tabs.sendMessage(tab.id, { type: 'URL Change'});
  }
});



// For updating the icon (RGB vs BW), tooltip, and whether the app stop rendering
chrome.tabs.onUpdated.addListener(function(tabId, changeInfo, tab) {
  if (isValidURL(tab.url)) {
    chrome.pageAction.setIcon({
      tabId: tabId,
      path: {
        '16': 'logo16.png',
        '24': 'logo24.png',
        '32': 'logo32.png',
      },
    }, () => {});
    chrome.pageAction.setTitle({
      tabId: tabId,
      title:'YouTube Night'
    });
  } else {
    chrome.tabs.sendMessage(tab.id, { type: 'Stop'});
    chrome.pageAction.setIcon({
      tabId: tabId,
      path: {
        '16': 'logo16_dimmed.png',
        '24': 'logo24_dimmed.png',
        '32': 'logo32_dimmed.png',
      },
    }, () => {});
    chrome.pageAction.setTitle({
      tabId: tabId,
      title:'Not available at this URL'
    });
  }
});

// Handle login requests from app
chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
  switch (request.type) {
    case "Login": {
      console.log('Login Request Received');
      chrome.identity.getAuthToken({interactive: true}, function(token) {
        globalToken = token;
        let init = {
          method: 'GET',
          async: true,
          headers: {
            Authorization: 'Bearer ' + token,
            'Content-Type': 'application/json',
          },
          'contentType': 'json',
        };
        let userProfileURL = 'https://www.googleapis.com/oauth2/v2/userinfo?access_token=' + token + '&access_token_type=bearer';

        switch (request.mode) {
          case 'Cache':
            fetch(userProfileURL, init).then((response) => response.json().then((data) => {
              sendResponse(JSON.stringify(data));
            }));
            break;

          case 'Interactive':
            let revokeTokenURL = 'https://accounts.google.com/o/oauth2/revoke?token=' + token;
            console.log(token);
            console.log(revokeTokenURL);
            fetch(revokeTokenURL).then(() => {
              chrome.identity.removeCachedAuthToken({token: token}, function () {
                fetch(userProfileURL, init).then((response) => response.json().then((data) => {
                  sendResponse(JSON.stringify(data));
                }));
              });
            });
            break;

          default:
            console.log("Error | Please specify a mode: 'Cache' or 'Interactive'");
        }
      });
      break;
    }

    case "Logout": {
      console.log('Logout Request Received');
      let revokeTokenURL = 'https://accounts.google.com/o/oauth2/revoke?token=' + globalToken;
      fetch(revokeTokenURL).then(() => {
        chrome.identity.removeCachedAuthToken({token: globalToken}, () => {
        });
      });
      sendResponse('Success');
      break;
    }

    default:
      break;
  }

  return true;
});
